# Word Games German Dataset

Code participation for https://github.com/szabgab/word-games-code 

Please note that ``word-games-code`` is deprecated. The software still works, and the data is still fetched from this repo, but there have been no improvements to the software for years. Creating your own word-games dataset would therefore be a waste of time at the moment.

## Keyboard 
        "de": [
            "qwertzuiopü",
            "  asdfghjklöä",
            "    yxcvbnmß"
        ],


## License
Attribution-ShareAlike 4.0 International (CC BY-SA 4.0) | Kate https://codeberg.org/kate 